# OnLatticeTS

This is the repository of OnLatticeTS, and agent-based on-lattice tumor simulator.

Currently, we are working on a new version of the framework. The previous source code is available at: http://users.nik.uni-obuda.hu/kissdani/3DTumorSimulator.zip